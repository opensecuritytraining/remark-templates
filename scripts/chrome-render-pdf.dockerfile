FROM node:13-slim
WORKDIR /tmp
RUN apt-get update && \
    env DEBIAN_FRONTEND=noninteractive apt-get install --yes git wget && \
    wget -O google-chrome.deb https://cloud.3mdeb.com/index.php/s/dXa46zbw2NdKnW8/download && \
    dpkg -i google-chrome.deb ; \
    env DEBIAN_FRONTEND=noninteractive apt-get install --yes -f && \
    apt-get clean && \
    rm google-chrome.deb
RUN git clone https://github.com/3mdeb/chrome-headless-render-pdf && \
    cd chrome-headless-render-pdf && \
    git checkout fe7d484b31b72f5707005cc147db4d30257af531 && \
    yarn install && \
    yarn build && \
    npm pack && \
    npm install -g chrome-headless-render-pdf*.tgz && \
    cd .. && \
    rm -rf chrome-headless-render-pdf
ENTRYPOINT [ "/usr/local/bin/chrome-headless-render-pdf" ]
