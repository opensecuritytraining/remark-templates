remark templates
----------------

## Prerequisites

### Markdown formatting

#### Front page

To create front page, at the top of the file we should have:

```
class: center, middle, intro

## DOCUMENT_TITLE

---
```

#### Page break

We should indicate in a markdown file, when given page should end. Following
syntax is used for page break:

```
---
```

#### Last page

To create last page, at the top of the file we should have:

```
class: center, middle, outro

## Title

---
```

## Usage

> Make sure to follow the [prerequisites](#prerequisites) section first.

### Create HTML file

Typically, you already have a markdown file which you would like to create PDF
from. Typically it is a repo of given training course.

* add `remark-templates` as submodule to this repo:

```
git submodule add git@gitlab.com:opensecuritytraining/remark-templates.git remark-templates
```

Assumed directory structure after this operation:

```
.
├── some-markdown-file.md
└── remark-templates
```

* copy the template markdown file from target templates directory, e.g.:

```
cp 3mdeb-training-presentation-template/3mdeb-training-presentation-template.md title-of-your-choice.md
```

* execute the `create-html.sh` script:

> training presentation template:
```
./remark-templates/scripts/create-html.sh 3mdeb-training-presentation-template "Arch4021 UEFI Introduction" uefi-intro.md
```

> pace enterprise presentation template:
```
./remark-templates/scripts/create-html.sh 3mdeb-pet-presentation-template "Arch4021 UEFI Introduction" uefi-intro.md
```

* OR use dockerized version of the script (e.g. MAC users):
```
./remark-templates/scripts/docker-create-html.sh 3mdeb-training-presentation-template "Title" title-of-your-choice.md
```

### Preview the HTML file

> Chrome / Chromium browser is recommended

* start the HTTP server:

  python3:
  ```
  python -m http.server 8080
  ```
  
  python 2:
  ```
  python -m SimpleHTTPServer 8080
  ```

* Enter localhost `http://127.0.0.1:8080/` and view the HTML file in browser

### Generate PDF file

#### Requirements

* `Docker` installed
* `python` or `python3` installed (for setting up the HTTP server)
* the `<title>` tag in each HTML file should be present - the printed filename
  will be based on that (spaces will be replaced with underscores)

#### Printing

* Go to the directory with your HTML files
* Script accepts as argument list of files and directories to print, directories
  are searched recursively for HTML files.
* Print everyting everything from current directory and subdirectories
```shell
./remark-templates/scripts/print-pdfs.sh .
```

* Print specific files
```shell
./remark-templates/scripts/print-pdfs.sh <path to html_file>
```

> HTML server's root is always located in current working directory, files must
> be located in working directory or it's subdirectories for printing to work.

* The HTML files will be verified and the HTTP server will be started

```shell
Checking for google-chrome...
google-chrome available
Checking for python3...
python3 available
Checking HTML files...
HTML files verified
Starting HTTP server...
```

* HTML files will be processed and the HTTP server will be stopped

```shell
Processing yocto_project_profiling_and_tracing.html
Printed pdf/25_Yocto_Project_Profiling_and_Tracing.pdf
Processing yocto_project_SDK.html
Printed pdf/21_Using_the_Yocto_Project_SDK.pdf
Stopping HTTP server...
HTTP server stopped
```

* The resulting HTML files could be found in the `pdf` subdirectory

## Available templates

All available templates are present in the root directory of this repository.
Refer to the templates description below.

### 3mdeb-training-presentation-template

Used for generating PDF presentation for training sessions.

### 3mdeb-pet-presentation-template

Used for generating PDF presentation for Pace Enterprise Training sessions.

## Code Usage

### Columns templates

For Custom design, depends on content we can you use 4 different columns settings.  Following
syntax is used for columns templates:

1 Centered-text Column
```
.column-1[
.middle-1[your-text]
]
```
2 Columns
```
.column-2[
.left-2[your-text]
.right-2[your-text]
]
```
3 Columns
```
.column-3[
.left-3[your-text]
.middle-3[your-text]
.right-3[your-text]
]
```
4 Columns
```
.column-4[
.left-4[your-text]
.left-middle-4[your-text]
.right-middle-4[your-text]
.right-4[your-text]
]
```
### Stardard Page

For standard page with list or text use this example:

Image with Title
```
.column-1-title[<img src='image_url/image.svg'
class='image-subtitle'>TITLE]
```
H1 - top right page Title
```
#Page_title
```
H5 title then list or text
```
#####List_Title
* list item
* list item
* list item
Text
```

## Known Issues

* When printing from the browser, the font on the first page may be too thick.
  - to fix this refresh the page or clear the cache
